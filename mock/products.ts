export interface Product {
  id: number;
  title: string;
  summary: string;
  price: number;
  stock: number;
  image: string;
}


export const MockProducts : Product[] = [{
  id: 54623,
  image: "https://images.pexels.com/photos/90946/pexels-photo-90946.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
  price: 17000,
  stock: 5,
  title: "محصول زیبا",
  summary: "این توضیحات یک محصول واقعا زیباست"
},
{
  id: 745321,
  image: "https://m.media-amazon.com/images/I/6158rlWa8ML._AC_SX466_.jpg",
  price: 17000,
  stock: 3,
  title: "یک محصول دیگر",
  summary: "این توضیحات یک محصول واقعا زیباست"
},
{
  id: 5641231,
  image: "https://cb2.scene7.com/is/image/CB2/TrioVasesWhiteSHF16/$web_pdp_main_carousel_xs$/190905021603/3-piece-trio-vase-set.jpg",
  price: 17000,
  stock: 5,
  title: "دوربین عکاسی",
  summary: "این توضیحات یک محصول واقعا زیباست"
},
{
  id: 123156412,
  image: "https://images.pexels.com/photos/90946/pexels-photo-90946.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
  price: 45000,
  stock: 5,
  title: "گدجت گرون",
  summary: "این توضیحات یک محصول واقعا زیباست"
},] 