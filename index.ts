import { MockProducts } from './mock/products';
import express, { RequestHandler } from 'express'
import {Client, QueryResult} from 'pg'
import cors from 'cors'
import bodyParser from 'body-parser'

const app = express()
const port = 7070
let client : Client;

app.use(cors())
app.use(bodyParser())

app.get<{}, {}, {}, {orderBy: number}>('/products', async (req, res) => {
  let result : QueryResult<any>;
  const orderBy = Number(req.query.orderBy)
  if (orderBy === 0) {
    result = await client.query(`select * from get_product('')`)
  } else if (orderBy === 2) {
    result = await client.query(`select * from get_product('asc')`)
  } else {
    result = await client.query(`select * from get_product('desc')`)
  }
  res.json(result.rows.map(({product_id, ...item}) => ({id: product_id, ...item})))
})


app.post<{}, {}, {id: number; qty: number}[]>('/cart', async (req, res) => {
  const body = req.body
  const cartQuery = await client.query('select * from create_cart(1)')
  const cartId = cartQuery.rows[0].create_cart
  for (const item of body) {
    await client.query(`select * from create_cart_item(${cartId}, ${item.id}, ${item.qty})`)
  }
  res.json({cartId})
})

app.post<{}, {}, {address: string; phoneNumber: string; cartId: number}>('/order', async (req, res) => {
  
  const {cartId, address, phoneNumber} = req.body
  const result = await client.query(`select * from create_order(${Number(cartId)}, '${address}', '${phoneNumber}')`)
  const id = result.rows[0].create_order
  await client.query(`call calculate_order_total_price(${id})`)
  
  res.status(200).send("success")

})

const server = app.listen(port, async () => {
  client = new Client({
    user: 'postgres',
    host: 'localhost',
    database: 'online_shop',
    password: '13777731',
    port: 5432,
  })
  await client.connect()

  console.log(`Example app listening at http://localhost:${port}`)
})

process.on('SIGTERM', () => {
  server.close(() => {
    client.end()
  })
})

